/* Author: Ajay Gejage

*/

//Accordion functionality
 $(document).ready(function(){  
  function accordion(){
    $(".accordion").on('click', function(){
      var acc = $(".accordion").not(this);
      if($(this).next().has("span")) {
        $(this).next().slideToggle("800ms");
        $(".accordion").not(this).next().slideUp("800ms");
        if(acc.hasClass("cross")){
          acc.removeClass("cross");
          acc.addClass("plus"); 
        }            
      }
      togglecross(this);      
    });
  }

  function togglecross(object) {
    if($(object).hasClass("plus")){
      $(object).toggleClass("plus");
    }else{
      $(object).addClass("plus"); 
    }
    $(object).toggleClass("cross");     
  }

  accordion();
});

// Navigation button functionality
var html = document.getElementsByTagName("html")[0];
var btn = document.getElementsByClassName("nav-btn")[0];
var nav = document.getElementsByTagName("nav")[0]; 
var ul = nav.getElementsByTagName("ul");
btn.addEventListener('click', function(event){
  if(nav.className == "" || nav.className == "navclose"){
    nav.className = "navdiv";
    btn.classList.add("btncross");
    html.classList.add("scroll-lock");
  }else{
    nav.className = "navclose";
    btn.classList.remove("btncross");  
    html.classList.remove("scroll-lock");
  }
});





















